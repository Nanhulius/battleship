import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        marginTop: 30,
        backgroundColor: 'green'
    },
    title: {
        fontSize: 50,
        marginTop: 40,
        textAlign: 'center'
    },
    footer: {
        marginBottom: 50,
        backgroundColor: 'green'
    },
    footerText: {
        fontSize: 18,
        textAlign: 'center'
    },
    board: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#44A7C4',
        flexDirection: 'row'
    },
    gameInfo: {
        textAlign: 'center',
        fontSize: 15,
    },
    flex: {
        flexDirection: 'row'
    },
    item: {
        margin: 10,
        padding: 5
    },
    button: {
        margin: 20,   
        padding: 10,
        backgroundColor: "#73CED6",
        borderRadius: 25,
        alignItems: 'center'
    },
    gameIconGroup: {
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 15,
        marginLeft: 25,
        justifyContent: 'space-evenly'
    },
    icon: {
        marginLeft: 25,      
    },
    gameStatus: {
        marginTop: 15,
        fontSize: 16,
        textAlign: 'center',
        justifyContent: 'center'
    },
    timerStatus: {
        fontSize: 25,
        textAlign: 'center',
        justifyContent: 'center'
    }
})