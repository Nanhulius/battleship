import React, { useState, useEffect } from 'react';
import { Text, View, Button, Pressable } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons'
import styles from '../style/Style';

const MISS = 'radiation'; //will be invisible intentionally
const WATER = 'water';
const SHIP = 'ship';
const COLUMNS = 5;
const ROWS = 5;
let initialBoard = new Array(COLUMNS * ROWS).fill(WATER);

export default function Board() {

    const [shipArray, setShipArray] = useState([]);
    const [hits, setHits] = useState(0);
    const [ships, setShips] = useState(3);
    const [bombsLeft, setBombsLeft] = useState(15);
    const [gameEnd, setGameEnd] = useState(false);
    const [status, setStatus] = useState("Game has not started");
    const [board, setBoard] = useState(initialBoard);
    const [gameStarted, setGameStarted] = useState(false);
    const [button, setButton] = useState("Start Game");

    const [timer, setTimer] = useState(30);
    const [timerInterval, setTimerInterval] = useState(null);

    //Building the Gameboard
    const COLUMNS = initialBoard.length / 5;
    const ROWS = initialBoard.length / 5;
    const items = [];
    // rows
    for (let x = 0; x < ROWS; x++) {
        const cols = [];
        // cols
        for (let y = 0; y < COLUMNS; y++) {
            cols.push(
                <Pressable
                    key={x * COLUMNS + y}
                    style={styles.item}
                    onPress={() => drawItem(x * COLUMNS + y)}>
                    <FontAwesome5
                        key={x * COLUMNS + y}
                        name={board[x * COLUMNS + y]}
                        size={32}
                        color={chooseItemColor(x * COLUMNS + y)}>
                    </FontAwesome5>
                </Pressable>
            )
        } // cols
        let row =
            <View key={"row" + x}>
                {cols.map((item) => item)}
            </View>
        items.push(row);
    } // rows

    // Checks the gameflow
    useEffect(() => {
        checkWin();
        if (timer === 30 && gameStarted == false) {
            setStatus('Click "Start new game"');
        }
        if (bombsLeft < 0) {
            setBombsLeft(bombsLeft - 1);
        }
        if (bombsLeft == 0) {
            stopTimer();
        }
        if (timer == 0) {
            stopTimer();
        }
    }, [bombsLeft, ships, timer]);

    // When icon-index is clicked check if there is a ship or not
    function drawItem(ship) {
        if (gameStarted == true && gameEnd == false) {
            if (shipArray.includes(ship) && board[ship] == SHIP) {
                setStatus("That ships has already been destroyed, bomb somewhere else!")
            }
            else if (shipArray.includes(ship) && board[ship] == WATER) {
                board[ship] = SHIP;
                setHits(hits + 1);
                setShips(ships - 1);
                setStatus("That was a hit! Keep bombing!");
                setBombsLeft(bombsLeft - 1);
            }
            else if (board[ship] == WATER) {
                if (ships == 1) {
                    setStatus("Ship evaded that bomb, keep bombing!");
                } else {
                    setStatus("Ships evaded that bomb, keep bombing!");
                }
                board[ship] = MISS;
                setBombsLeft(bombsLeft - 1);
            }
            else {
                setStatus("No need to bomb empty sea, bomb the ships!");
            }
        }
        else if (gameEnd == true) {
            setStatus("Game ended. Start a new one?");
        }
    }

    // Setting color for icons
    function chooseItemColor(icon) {
        if (board[icon] === SHIP) {
            return "#FF3031";
        }
        else if (board[icon] === MISS) {
            return "#44A7C4";
        }
        else {
            return "#003366";
        }
    }

    // Check if the win/lose conditions are met
    function checkWin() {
        if (ships == 0 && bombsLeft == 0) {
            setStatus("You destroyed the last ship with your last bomb, you win!");
            endGame();
        }
        else if (ships == 0) {
            setStatus("All ships destroyed, you win!");
            endGame();
            stopTimer();
        }
        else if (ships == 3 && bombsLeft == 0) {
            setStatus("You missed all the bombs, one kind of talent. You lose!");
            endGame();
        }
        else if (ships == 2 && bombsLeft == 0) {
            setStatus("You managed to sink only one ship, you lose!");
            endGame();
        }
        else if (ships == 1 && bombsLeft == 0) {
            setStatus("One ship escaped, you lose!");
            endGame();
        }
        else if (timer == 0) {
            setStatus("Time is up! You lose...");
            endGame();
        }
        else {
            return '';
        }
    }

    // After win/lose conditions are met, reset these variables so "New game"-button works properly
    function endGame() {
        setGameEnd(true);
        setGameStarted(false);
        setButton("New game");
        setShipArray([]);
    }

    // Starting the timer
    function startTimer() {
        if (timer > 0) {
            const time = setInterval(() => setTimer(timer => timer - 1), 1000);
            setTimerInterval(time)
        }
    }

    // Stopping the timer
    function stopTimer() {
        clearInterval(timerInterval);
    }

    // Putting ships in an array with random number
    function addShips() {
        for (let i = 0; i < 3;) {
            let ship = Math.floor(Math.random() * 24);
            if (!shipArray.includes(ship)) {
                shipArray.push(ship);
                i++;
            }
        }
        console.log(shipArray);
    }

    // Handling the "Start game"/"New game" -button
    function handleButton() {
        if (gameEnd == false) {
            if (gameStarted == true) {
                setStatus("You already started the game, bomb the ships!");
            }
            else { // Starts game
                startTimer();
                addShips();
                setStatus("Game is on, bomb the ships!");
                setGameStarted(true);
            }
        } // Resets the board and starts a new game
        else if (gameEnd == true) {
            let initialBoard = [...board];
            initialBoard = new Array(COLUMNS * ROWS).fill(WATER);
            setBoard(initialBoard)
            setShips(3);
            setBombsLeft(15);
            setHits(0);
            setTimer(30);
            setGameEnd(false);
            setGameStarted(true);
            setButton("New Game");
            setStatus("Game is on, bomb the ships!");
            startTimer();
            addShips();
        }
    }

    return (
        <>
            <View style={styles.board}>
                <View style={styles.flex}>{items}</View>
            </View>
            <View>
                <Text style={styles.gameStatus}>{status}</Text>
            </View>
            <View style={styles.button}>
                <Button style={styles.gameInfo} onPress={handleButton} title={button} />
            </View>
            <View style={styles.gameInfo}>
                <Text style={[styles.gameInfo, styles.gameIconGroup]}>
                    <FontAwesome5 style={styles.icon} name="ship" size={20} color="#FF3031" /> {hits}
                    <FontAwesome5 style={styles.icon} name="bomb" size={20} /> {bombsLeft}
                    <FontAwesome5 style={styles.icon} name="ship" size={20} /> {ships}
                </Text>
            </View>
            <View style={styles.gameInfo}>
                <Text style={[styles.gameInfo, styles.timerStatus]}>
                    <FontAwesome5 style={styles.gameIconGroup} name="clock" size={25} /> {timer}
                </Text>
            </View>
        </>
    )
}
