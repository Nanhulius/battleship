import React from 'react';
import { Text, View } from 'react-native';
import Header from './components/Header';
import Footer from './components/Footer';
import Board from './components/Board';
import styles from './style/Style';

export default function App() {
  return (
    <View style={styles.container}>
      <Header />
      <Board />
      <Footer />
    </View>
  );
}
